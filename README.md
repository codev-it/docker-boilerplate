[![Logo](https://www.codev-it.at/logo.png)](https://www.codev-it.at)

# Codev-IT Docker Boilerplate's

[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://bitbucket.org/codev-it/docker-boilerplate/src/master/LICENSE)

## Description

This repository contains a collection of Docker boilerplate templates designed by Codev-IT. These templates are intended to streamline the process of setting up Docker environments for various applications.

## Templates

### [Default](https://bitbucket.org/codev-it/docker-boilerplate/src/master/default)

This Docker project provides a robust setup for various Docker services like MySQL and similar services.
It includes a custom build process and health checks to ensure smooth operation.

### [Supervisor](https://bitbucket.org/codev-it/docker-boilerplate/src/master/supervisor)

This extended Docker project incorporates Supervisor to manage multiple services within the same container,
enhancing the original setup that supports various Docker services like MySQL.
Supervisor ensures that all services are running smoothly and restarts them if necessary.

## Getting Started

To use these templates, clone this repository and navigate to the desired template directory.
Customize the Dockerfile and associated scripts according to your project's requirements.
Build and run your Docker container using the standard Docker commands.

```shell
git clone https://bitbucket.org/codev-it/docker-boilerplate.git
cd [template-directory]
docker build -t service-name .
docker run service-name
```

## Contributing

Contributions are welcome. Please read our contributing guidelines and submit pull requests to our repository.

## Donations

Your support is appreciated! You can make a donation using the following
methods:

### Stripe

For donations via credit or debit card, please visit our Stripe donation page:
[Donate with Stripe](https://donate.stripe.com/7sIaFa9DK18s8Ra8ww)

### PayPal

To donate using PayPal, please use the following link:
[Donate with PayPal](https://www.paypal.com/donate/?hosted_button_id=2GKPNJBQTAB3Y)

### Cryptocurrency

We also accept donations in cryptocurrency. Please send your contributions to
our crypto account:

- **Bitcoin (BTC):** `bc1qadle9lzzv5plw98n0gndycx0dta2zyca9rmaxe`
- **Ethereum (ETH):** `0xf7a42B686437FA1b321708802296342D582576a0`

## Contact

For any inquiries, please contact us
at [office@codev-it.at](mailto:office@codev-it.at).
