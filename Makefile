# Codev-IT
# Docker multiple container
#
DOCKER_ID=codevit
DOCKER_TAG=latest
DOCKER_FILE=Dockerfile

.PHONY: help build test push shell run start stop logs clean release

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  build	     		build docker image"
	@echo "  build-all	   		build all folders"
	@echo "  push		   		push to docker hub"
	@echo "  push-all	   		push all to docker hub"
	@echo "  detach		   		run container in detach mode"
	@echo "  detach-all	   		run all container in detach mode"
	@echo "  exec		   		exec command in container"
	@echo "  exec-all	   		exec command in all container"
	@echo "  logs		   		get logs"
	@echo "  logs-all	   		get all logs"
	@echo "  stop		   		remove container"
	@echo "  stop-all	   		remove all container"
	@echo "  remove		   		remove container"
	@echo "  remove-all	   		remove all container"

build:
	@docker build \
		-t "$$(echo "${DOCKER_ID}/${DOCKER_NAME}:${DOCKER_TAG}" \
			| tr '[:upper:]' '[:lower:]')" \
			--build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
			--build-arg VCS_REF=`git rev-parse --short HEAD` \
			--build-arg VERSION="${DOCKER_TAG}" \
			${BUILD_ARG} \
			"${PWD}";

build-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make build DOCKER_NAME="$${build##*/}" PWD="$${PWD}/$${build##*/}"; \
		fi \
	done

push:
	@docker push ${DOCKER_ID}/${DOCKER_NAME}:${DOCKER_TAG}

push-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make push DOCKER_NAME="$${build##*/}"; \
		fi \
	done

detach:
	@docker run -id --rm --name ${DOCKER_NAME} $(PARMS) $(OPTS) $(DNS) $(VOLUMES) $(ENV) ${DOCKER_ID}/${DOCKER_NAME}:${DOCKER_TAG} $(CMD)

detach-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make detach DOCKER_NAME="$${build##*/}"; \
		fi \
	done

exec:
	@docker exec -i ${DOCKER_NAME} $(CMD)

exec-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make exec DOCKER_NAME="$${build##*/}"; \
		fi \
	done

logs:
	@docker logs $(PARMS) ${DOCKER_NAME}

logs-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make logs DOCKER_NAME="$${build##*/}"; \
		fi \
	done

stop:
	@docker stop ${DOCKER_NAME}

stop-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make detach DOCKER_NAME="$${build##*/}"; \
		fi \
	done

remove: stop
	@docker rm -f ${DOCKER_NAME}

remove-all: stop-all
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make remove DOCKER_NAME="$${build##*/}"; \
		fi \
	done

release: build push

release-all: build-all push-all