[![Logo](https://www.codev-it.at/logo.png)](https://www.codev-it.at)

# Codev-IT Docker Boilerplate

[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://bitbucket.org/codev-it/docker-boilerplate/src/master/default/LICENSE)

## Description

This Docker project provides a robust setup for various Docker services like
MySQL and similar services.
It includes a custom build process and health checks to ensure smooth operation.

## Builder Image

The Dockerfile for the builder image begins with FROM wodby/alpine:3 as builder,
which sets up an Alpine-based environment for building the service.

**Key steps include:**

* Setting up a working directory at /tmp.
* Creating a directory /src/usr/local/bin and copying build scripts there.
* Adding custom scripts like whereis_copy, docker-entrypoint.sh, and
  docker-healthcheck.sh.
* Setting executable permissions on these scripts.

## Final Image

The final Docker image is based on alpine:3.

**Key configurations:**

* Setting environment variables and creating a user and group for the build
  process.
* Copying the contents from the builder image.
* Installing necessary packages like tzdata and su-exec.
* Setting the entry point and health check commands.

## Metadata and Labels

The Dockerfile includes metadata as per http://label-schema.org, with labels for
build date, name, description, version, and more.
Entry Script

## Entrypoint Script

**The entry script (docker-entrypoint.sh):**

* Includes a debug mode.
* Sets a default Docker socket.
* Contains a helper function _gotpl.
* Adds the build user to the Docker group based on the existing Docker socket.
* Executes the command as a different user using su-exec.

## Health Check Script

**The health check script (docker-healthcheck.sh):**

* Includes a debug mode.
* Checks for the main process and returns a success or unhealthy status based on
  its existence.

## Usage

**To use this Docker setup:**

Build the image using the Dockerfile.
Run the container with necessary environment variables and mount points.

````shell
docker build -t service-name .
docker run service-name
````

## Customization

**To customize the build:**

* Modify the builder image steps as needed.
* Add or change scripts in the /bin directory.
* Update environment variables and labels for specific use cases.

## Contributing

Contributions are welcome. Please read our contributing guidelines and submit
pull requests to our repository.

## Donations

Your support is appreciated! You can make a donation using the following
methods:

### Stripe

For donations via credit or debit card, please visit our Stripe donation page:
[Donate with Stripe](https://donate.stripe.com/7sIaFa9DK18s8Ra8ww)

### PayPal

To donate using PayPal, please use the following link:
[Donate with PayPal](https://www.paypal.com/donate/?hosted_button_id=2GKPNJBQTAB3Y)

### Cryptocurrency

We also accept donations in cryptocurrency. Please send your contributions to
our crypto account:

- **Bitcoin (BTC):** `bc1qadle9lzzv5plw98n0gndycx0dta2zyca9rmaxe`
- **Ethereum (ETH):** `0xf7a42B686437FA1b321708802296342D582576a0`

## Contact

For any inquiries, please contact us
at [office@codev-it.at](mailto:office@codev-it.at).
