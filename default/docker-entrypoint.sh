#!/bin/sh

# debug
if [ "${DEBUG}" = true ]; then
  set -eux
else
  set -e
fi

# default
DOCKER_SOCKET=${DOCKER_SOCKET:-/var/run/docker.sock}

# helpers
_gotpl() {
  if [ -f "/etc/gotpl/$1" ]; then
    gotpl "/etc/gotpl/$1" >"$2"
  fi
}

# add user to docker group by existing docker socket
if [ -S "${DOCKER_SOCKET}" ]; then
  DOCKER_GID=$(stat -c '%g' "${DOCKER_SOCKET}")
  addgroup -S -g "${DOCKER_GID}" docker
  adduser "${BUILD_USER}" docker
  BUILD_GROUP=docker
fi

# start diverent user
su-exec "${BUILD_USER}":"${BUILD_GROUP}" "${@}"
