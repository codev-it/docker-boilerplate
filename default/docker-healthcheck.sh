#!/bin/sh

# debug
if [ "${DEBUG}" = true ]; then
  set -eux
else
  set -e
fi

# healthckeck
if [ "$(ps aux | pgrep -n main)" != "" ]; then
  echo "success"
  exit 0
fi
echo "unhealthy"
exit 1
